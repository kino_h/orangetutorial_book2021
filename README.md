# README

Copyright (c) 2017-2021, Hiori Kino

All rights reserved.

This software is released under the BSD License (3-clause BSD License). 

# video explanation

- A.2節 「Orange用CSVフォーマットへの変換」
https://youtu.be/z2X1jRdqUus

- 3.3節
https://youtu.be/JWIK0b5ERQQ

- 3 4節から3 4 3節まで
https://youtu.be/tkpI106pJgM

- 3 4 4節から3 5節まで 
https://youtu.be/ik4yVd1erD4
