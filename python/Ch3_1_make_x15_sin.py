import numpy as np
import pandas as pd


def make_data(filename, x, scale=0.001, normalize=False):
    """make a toy model

    Args:
        filename (str): filename to save
        x (np.array): a list of x1
        scale (float, optional): scale of gaussian noise. Defaults to 0.001.

    Returns:
        pd.DataFrame: data
    """
    x2 = x*x
    x3 = x**3
    x4 = x**4
    x5 = x**5
    x6 = np.sin(x)
    y = np.sin(x) + np.random.normal(scale=scale, size=x.shape[0])

    df = pd.DataFrame({"x1": x, "x2": x2, "x3": x3,
                       "x4": x4, "x5": x5, "sinx": x6, "y": y})
    if normalize:
        X = scaler.fit_transform(df.values)
        df = pd.DataFrame(X, columns=df.columns)

    df.to_csv(filename, index=False)
    return df


np.random.seed(seed=1)
x = np.linspace(0.0, 5.0, 100)
filename = "x15_sin.csv"
df = make_data(filename, x)

x_new = np.linspace(5.0, 6.0, 20)
filename_new = "x15_sin_new.csv"
df_new = make_data(filename_new, x_new)
