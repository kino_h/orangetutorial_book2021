import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler


def make_data(filename, x1, scale=0.001, normalize=True):
    """make a toy model

    Args:
        filename (str): filename to save
        x1 (np.array): a list of x1
        scale (float, optional): scale of gaussian noise. Defaults to 0.001.
        normalize (bool, optional): a flag of normalization. Defaults to True.

    Returns:
        pd.DataFrame: data
    """

    scaler = MinMaxScaler()
    x2 = np.sin(5*x1+0.4)
    df12 = pd.DataFrame({"x1": x1, "x2": x2})
    x12 = scaler.fit_transform(df12.values)
    x1 = x12[:, 0]
    x2 = x12[:, 1]
    x3 = x1+x2
    y = x3 + np.random.normal(scale=scale, size=x1.shape[0])
    df = pd.DataFrame({"x1": x1, "x2": x2, "x3": x3, "y": y})
    if normalize:
        X = scaler.fit_transform(df.values)
        df = pd.DataFrame(X, columns=df.columns)
    df.to_csv(filename, index=False)
    return df


np.random.seed(seed=1)
x1 = np.linspace(0, 1, 50)
scale = 0.001
filename = "x123.csv"
df = make_data(filename, x1, scale=scale)
