#!/usr/bin/env python
# coding: utf-8
import os
import numpy as np
from sklearn import   datasets
import pandas as pd

def load_Xy():
    digits = datasets.load_digits(n_class=6)
    Xraw = digits.data
    y = digits.target
    # normalize X
    xmax = Xraw.ravel().max()
    X =  Xraw/xmax
    return X,y 

def make_df(X, y):
    n_features = X.shape[1]
    # make X Table
    label = np.array([str(i) for i in range(n_features)])
    vartype = np.array(["continuous" for i in range(n_features)]).reshape(1,-1)
    classtype = np.array(["" for i in range(n_features)]).reshape(1,-1)
    Xtable = np.vstack( (vartype, classtype , X) )
    dfX = pd.DataFrame(Xtable, columns= label)

    # make y table
    label = np.array(["y"])
    vartype = np.array(["discrete" ]).reshape(1,-1)
    classtype = np.array(["class"]).reshape(1,-1)
    ylabel = np.array(list(map(str,y))).reshape(-1,1)
    ytable = np.vstack( (vartype, classtype , ylabel) )
    dfy = pd.DataFrame(ytable, columns=label)
    df = pd.concat([dfX, dfy], axis=1)

    return df

X, y = load_Xy()

df = make_df(X[:100,:],y[:100])
filename = "digits100.csv"
df.to_csv(filename, index=False)
print(filename, "is made.")

df = make_df(X[100:200,:],y[100:200])
filename = "digits100+.csv"
df.to_csv(filename, index=False)
print(filename, "is made.")


df = make_df(X[:800,:],y[:800])
filename = "digits800.csv"
df.to_csv(filename, index=False)
print(filename, "is made.")

df = make_df(X[800:,:],y[800:])
filename = "digits800+.csv"
df.to_csv(filename, index=False)
print(filename, "is made.")

print("current directory",os.getcwd())
print("Please copy them to 'your repository'/data_generated/ directory.")



